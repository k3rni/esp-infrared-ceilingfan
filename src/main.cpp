#include <stdint.h>
#include <Arduino.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <BluetoothSerial.h>

const uint16_t kIrLed = 15;

IRsend transmitter(kIrLed);
BluetoothSerial SerialBT;

void setFanSpeed(uint8_t speed) {
  const uint16_t codes[4] = {0xC10, 0xC01, 0xC04, 0xC43};
  Serial.printf("Setting fan speed to %d with code %x\r\n", speed, codes[speed]);
  transmitter.sendSymphony(codes[speed], 12, 2);
  delay(100);
}

void setLights(uint8_t onoff) {
  const uint16_t codes[2] = {0xC20, 0xC08};
  Serial.printf("Setting lights to %d with code %x\r\n", onoff, codes[onoff]);
  transmitter.sendSymphony(codes[onoff], 12, 2);
  delay(100);
}

/* Per http://www.hifi-remote.com/sony/Sony_tv.htm#tvb
   and https://github.com/probonopd/irdb/tree/master/codes/Sony/TV
   the codes are (dev.subdev func = Hex):
   power off = 1 47 = 0xF50
   power on 1 46 = 0x750
   cursor up = 1 116 = 0x2F0
   cursor down = 1 117 = 0xAF0
   cursor left = 1 52 = 0x2D0
   cursor right = 1 51 = 0xCD0
   ok/enter = 1 101 = 0xA70
   heart/fav = 119 118 = 0x37EE
   back 151 35 = 0x62E9
   AV1 1 64 = 0x030
   AV2 1 65 = 0xA30
   AV3 1 66 = 0x430
   VGA 1 67 = 0xC30
   Component 164 23 = 0x7425
   HDMI1 = 26 90 = 0x2D58
   HDMI2 = 26 91 = 0x6D58
   HDMI3 = 26 92 = 0x1D58
   HDMI4 = 26 93 = 0x5D58
*/

void tvBasic(size_t index) {
  const uint16_t codes[] = {
    0xF50, // power off
    0x750, // power on
    0x2F0, // cursor-up
    0xAF0, // cursor-down
    0x2D0, // cursor-left
    0xCD0, // cursor-right
    0xA70, // Enter/OK
    0x290, // mute
  };
  if (index < 0 || index > 7) return;
  Serial.printf("Sending tv code %d = %x\n\r", index, codes[index]);
  transmitter.sendSony(codes[index], 12, 2);
  delay(100);
}


void tvInput(size_t index) {
  const uint16_t codes[] = {
    0x2D58, // HDMI1
    0x6D58, // HDMI2
    0x1D58, // HDMI3
    0x5D58, // HDMI4
    0x7425, // Component
    0xC30, // VGA
  };
  if (index < 0 || index > 5) return;
  Serial.printf("Sending tv code %d = %x\n\r", index, codes[index]);
  transmitter.sendSony(codes[index], index < 5 ? 15 : 12, 2);
  delay(100);
}

void tvVolume(uint8_t dir, uint8_t steps) {
  const uint16_t codes[] = {
    0x490, // Vol+
    0xC90, // Vol-
  };
  if (dir < 0 || dir > 1) return;
  Serial.printf("Adjusting tv volume dir=%d for %d steps\r\n", dir, steps);
  transmitter.sendSony(codes[dir], 12, (steps + 1) * 2);
}

void bt_callback(esp_spp_cb_event_t event, esp_spp_cb_param_t *param) {
  switch (event) {
    case ESP_SPP_SRV_OPEN_EVT:
      Serial.println("Connected");
      break;
    case ESP_SPP_CLOSE_EVT:
      Serial.println("Disconnected");
      break;
  }
}


void setup() {
  transmitter.begin();
  Serial.begin(115200);
  SerialBT.register_callback(bt_callback);
  if (!SerialBT.begin("FanSerial")) {
    Serial.println("Failed setting up bluetooth");
  } else {
    Serial.println("Bluettoth up");
  }
}

void loop() {
  if (!Serial.available() && !SerialBT.available()) {
    delay(1000);
    return;
  }

  String command;
  if (Serial.available())
    command = Serial.readString();
  else if (SerialBT.available())
    command = SerialBT.readString();

  command.trim(); // modifies in-place

  const char *s = command.c_str();

  while (*s) {
    Serial.printf("%c\r\n", *s);
    switch (*s) {
    case '0': case '1': case '2': case '3':
      setFanSpeed(*s - '0');
      break;
    case 'L':
      setLights(*(++s) - '0');
      break;
    case 'T':
      tvBasic(*(++s) - '0');
      break;
    case 'Y':
      tvInput(*(++s) - '0');
      break;
    case '+':
      tvVolume(0, *(++s) - '0');
      break;
    case '-':
      tvVolume(1, *(++s) - '0');
      break;
    case 'P':
      delay(100);
      break;
    }
    s++;
  }
}

