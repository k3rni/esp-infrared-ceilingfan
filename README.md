# Purpose

This project is a Bluetooth-based remote control for a couple devices that I wished to control from a PC. Read the companion article at [blog.rebased.pl](https://blog.rebased.pl/2021/04/20/light-fixture-bluetooth.html) for details and board schematics.

# Requirements

1. Recent version of Python
2. Recent version of PlatformIO
3. A compatible microcontroller board. This is for an Espressif ESP32 Dev module, but is easily adapted to many others.
4. An IR emitter circuit. Plugging the IR LED directly into digital outputs will likely not work. The digital pin should drive a transistor's base through a resistor, the LED should be connected to the emitter, and voltage from the board goes to its collector.

# Compiling and uploading

    To compile:
    $ platformio run
    To compile and upload:
    $ platformio run -t upload

# Usage over a serial terminal

Connect with any serial terminal, using 115200,8N1 setup.
Type a single command, confirm with Enter.

Commands are:

* `0`, `1`, `2`, `3`: Emit Westinghouse ceiling fan codes to set speed. 0 is off, 3 is maximum.
* `L0` and `L1`: Turn Westinghouse ceiling fan's built-in lights off and on respectively.

# Shell functions

Requires `socat`, from your distro's package of the same name.

```bash
fan() { echo $1 | socat -t2 - file:/dev/ttyUSB0,b115200 }
lights() { echo L$1 | socat -t2 - file:/dev/ttyUSB0,b115200 }
```

Add to your bashrc, then just type e.g. `fan 0` on the command line.

# Shortcut keys

If your desktop environment allows assigning shortcuts to arbitrary commands, a very similar form can be used. Just extract whatever's inside braces in the shell functions, put it between single quotes, and use `bash -c` to run it.

```bash
# Lights on
/bin/bash -c 'echo L1 | socat -t2 - file:/dev/ttyUSB0,b115200'
```

# Bluetooth

If supported on your board, Bluetooth may be enabled. There are no special programs needed on the client side, any Bluetooth serial terminal will do.

It's even possible to talk to the device with `socat`!.

1. Install `bluez-utils`, ensure `bluetoothctl` is available
2. `bluetoothctl list` will show your host controllers (Bluetooth adapters, either built-in or connected typically over USB). Note its MAC address.
3. `bluetoothctl devices` will show available devices. This remote controller shows up as "FanSerial". Note its MAC address.

Now, you'll need to transcribe both MAC addresses in reverse, but keeping each two-digit pair as-is.
For example, if the address was `A1:B2:C3:D4:E5:F6`, then the reverse is `F6E5D4C3B2A1`.

```bash
fanbt() { echo $1 | socat -t2 - SOCKET-CONNECT:31:3:xDEVICE_ADDRESS_REVERSEDx0100,bind=xHOST_ADDRESS_REVERSEDx0100 }
```

Substitute `DEVICE_ADDRESS_REVERSED` and `HOST_ADDRESS_REVERSED` for the appropriate addresses, notated in reverse as shown before.

## Explanation

Socat's `SOCKET-CONNECT` mode allows connecting over anything that your kernel supports.
The first argument (`31`) is the protocol family, or domain; in this case it's AF_BLUETOOTH.
If you look inside `/usr/include/bluetooth/bluetooth.h` (with `bluez-libs` installed) and search for its name, you'll see the value.
Right next to it is the second argument, which is the protocol, in this case `BTPROTO_RFCOMM`, equal to 3.
The third argument is a socat-specific representation of a sockaddr structure. In it, `x` is the field separator, and the first field is the device MAC address.
It's required to be entered in reverse order (or more specifically, LSB notation). The next field is the channel, in this case we're using channel 1 but in LSB notation.

To actually connect over Bluetooth, we also need to specify a bind address, which is the address of a local device which is to initiate the connection.
This is of course the host controller MAC address, again in LSB order, followed by channel 1.

# Extras: not only the fan

I'm now using this to also drive some other devices, besides the fan/light. Notably, my (somewhat ancient now) Sony Bravia TV.
The code handles several new TV-specific commands, prefixed with `T` for some basic controls, `Y` for input selection and `-` or `+` for volume adjustment.

Additionally, a single input line may now contain more than one command. Just concatenate them together. Characters that are not part of a valid command are simply ignored. To facilitate chaining, there is now a `P`ause command, which takes no arguments and waits 100ms.

## Tools

When looking for codes for driving the TV, I encountered a lot of sources in different formats.
[IRDB](https://github.com/probonopd/irdb/tree/master/codes) lists them as device-function pairs, while other sources like [RemoteCentral](http://www.remotecentral.com) show them as Pronto codes.
Neither is directly usable with IRremoteESP8266, so I created converters for both these formats. Find them as Python scripts under `tools/`. They have no dependencies outside stdlib.


## Shell functions

Functions described above, including ones for driving the TV, can be found under `shell/`. Use these as a starting point for your customizations.
