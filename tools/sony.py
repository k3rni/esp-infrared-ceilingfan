#! /usr/bin/env python

"""
Usage: sony.py LEN DEV[.SUBDEV] COMMAND

Converts a Sony12, Sony15 or Sony20 code to bit representation.

LEN is 12, 15 or 20, depending on your device's protocol.
DEV is device number, usually just 1. When using 20 bit lengths, it can also be a DEV.SUBDEV pair.
COMMAND is the button function code, according to Sony manuals.
For a list, consult IRDB at https://github.com/probonopd/irdb/tree/master/codes/Sony

Displays a snippet ready to use in IRRemote8266 code.
"""

from sys import argv, stderr, exit

FORMATS = {
    12: "{dev:05b}{func:07b}",
    15: "{dev:08b}{func:07b}",
    20: "{subdev:08b}{dev:05b}{func:07b}"
}

if __name__ == "__main__":
    if len(argv) < 4:
        stderr.write(__doc__)
        exit(1)

    numbits, func = int(argv[1]), int(argv[3])
    dev, _, subdev = argv[2].partition('.')
    device = int(dev)
    subdevice = 0 if subdev == '' else int(subdev)

    fmt = FORMATS[numbits]

    bits = fmt.format(func=func, dev=device, subdev=subdevice)[::-1]
    print("Raw bits: {}".format(bits))
    print("IRRemote: sendSony({:#x}, {}, 2)".format(int(bits, 2), numbits))
