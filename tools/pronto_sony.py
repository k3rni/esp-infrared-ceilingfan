# /usr/bin/env python3

"""
Decodes Sony-flavor pronto codes from stdin.

Outputs general info, and the parsed code in several useful formats.
"""

import sys
from itertools import zip_longest
from functools import reduce
from enum import Enum, auto


class CodePosition(Enum):
    """Helper entities to encode lead-in and lead-out in decoded sequence."""

    LEAD_IN = auto()
    LEAD_OUT = auto()


def grouper(iterable, n, fillvalue=None):
    """Collect data into fixed-length chunks or blocks."""
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def parse_pronto(code, demodulator):
    """Parse a Pronto code and display some info."""
    # A Pronto Hex code is a series of space-separated, four-digit hex values.
    words = [int(v, 16) for v in code.split()]

    # Starting with a preamble in the first four
    preamble, words = words[:4], words[4:]

    # The first word is always zero and not interesting.
    # The second one encodes carrier frequency. For Sony devices it's either 38 or 40kHz,
    # but expressed in a Pronto-specific formula.
    # Finally, there are two code lengths. First one for the press sequence to send initially,
    # and then for the repeat sequence - when the button is held.
    _, pronto_freq, press_len, rep_len = preamble

    # Sony codes use only the repeat part.
    assert press_len == 0, "This is probably not a Sony code: should only contain the repeat sequence."

    # Sony codes use a length of 12, 15 or 20 bits. Plus one for the lead-in.
    assert rep_len in (13, 16, 21), "This is probably not a Sony code: wrong length."

    # Sony uses a carrier of 38kHz or 40kHz, depending on device.
    carrier = 1e6 / (pronto_freq * 0.241246)
    assert abs(carrier - 38_000) < 380 or abs(carrier - 40_000) < 400, \
           "This code uses wrong carrier frequency. It's probably not for Sony devices."
    print("Carrier: {:.3f}kHz".format(carrier / 1e3))

    # Next, there is zero bits of press sequence, followed by the repeat sequence.
    # Each bit is represented by a pair of (carrier-off, carrier-on) timings.
    rep = [demodulator(on, off) for on, off in grouper(words, 2)]
    # Drop the lead-in
    rep.pop(0)

    raw = ''.join('' if code == CodePosition.LEAD_IN else str(code)
                  for code in rep)
    print("Raw bits: {}".format(raw))

    packed = pack_bits(rep)
    # IRRemote-recommended repeat-count for Sony remotes.
    print("IRremote: sendSony({:#x}, {}, 2)".format(packed, rep_len - 1))

    print("Sony{}: {}".format(rep_len - 1, dev_func(rep)))


def pack_bits(seq):
    """Convert an LSB-first sequence of bit values to an int."""
    # The code is sent LSB-first, so needs reversing.
    return reduce(lambda acc, bit: (acc << 1) | bit, seq[::-1], 0)


def dev_func(seq):
    """Split an LSB-first code sequence into a command and device code."""
    subdev = None

    # Function code is always the least-significant 7 bits.
    # In this LSB-first representation, it's at the start.
    func = pack_bits(seq[:7])

    if len(seq) == 20:
        # Sony20 codes have a 5-bit device and 8-bit subdevice.
        dev = pack_bits(seq[7:12])
        subdev = pack_bits(seq[12:])
    else:
        # Sony12 and Sony15 don't have a subdevice.
        dev = pack_bits(seq[7:])

    if subdev is not None:
        return "dev {}.{} func {}".format(dev, subdev, func)
    else:
        return "dev {} func {}".format(dev, func)


# Per specification, Sony codes use two timings: space is 600µs and mark is 1200µs.
# These correspond to Pronto timings of 24 and 48 cycles (of a 25µs clock period) respectively,
# or 0x18 and 0x30 hex. A space-space sequence represents a 0 bit, and a mark-space represents 1.
# However, many files found in the wild differ by tolerable margins.
def is_space(duration):
    return abs(duration - 0x18) < 3


def is_mark(duration):
    return abs(duration - 0x30) < 4


def sony_timings(on, off):
    if is_space(on) and is_space(off):
        return 0
    elif is_mark(on) and is_space(off):
        return 1
    elif is_mark(on // 2) and is_space(off):
        return CodePosition.LEAD_IN
    elif off > 0x60:
        # This is a lead-out. However, for Sony, it actually doubles as a data bit,
        # depending on the on-time.
        return 1 if is_mark(on) else 0
    raise ValueError("Unknown on-off pair {},{}".format(on, off))


if __name__ == "__main__":
    code = sys.stdin.readline()
    parse_pronto(code.strip(), sony_timings)
