# Replace REMOTE_MAC with your ESP32's Bluetooth MAC, transcribed backwards like strtoba()
# For example, if the address was A1:B2:C3:D4:E5:F6, enter F6E5D4C3B2A1
# Same for HOST_MAC, using the address of your host adapter. Obtain it with e.g. `bluetoothctl list`
btsendremote() { socat -t2 - 'SOCKET-CONNECT:31:3:xREMOTE_MACx0100,bind=xHOST_MACx0100' }

# Usage: fan 0|1|2|3|4
# Send fan-speed command from remote
fan() { echo $1 | btsendremote }

# Usage: lights 0|1
# Send lights on-off command from remote
lights() { echo L$1 | btsendremote }

# Usage: sonytv COMMAND
# NOTE: not all commands handled by remote are used here. Notably, this omits
# navigation and volume setting.
sonytv() {
    case $1 in
        on)
            echo T1 | btsendremote
            ;;
        off)
            echo T0 | btsendremote
            ;;
        xsx)
            echo Y0 | btsendremote
            ;;
        kodi)
            echo Y1 | btsendremote
            ;;
        x360)
            echo Y4 | btsendremote
            ;;
        vga)
            echo Y5 | btsendremote
            ;;
        mute)
            echo T7 | btsendremote
            ;;
    esac
}
